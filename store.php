<?php
session_start();

        $user='root';
        $password='';
        $error='';
         
        $connection = new PDO('mysql:host=localhost;dbname=khairul', $user, $password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//if($_SERVER['REAUEST_METHOD']=='POST')
//{
        $username = $_POST['user_name'];
        $userjob = $_POST['user_job'];

        $imgFile = $_FILES['user_image']['name'];
        $tmp_dir = $_FILES['user_image']['tmp_name'];
        $imgSize = $_FILES['user_image']['size'];
        
        

            if(empty($username)){
             $_SESSION['error_msg'] = "Please Enter Username.";
              header('location:create.php');
            }
            else if(empty($userjob)){
             $_SESSION['error_msg'] = "Please Enter Your Job Work.";
              header('location:create.php');
            }
            else if(empty($imgFile)){
            $_SESSION['error_msg'] = "Please Select Image File.";
             header('location:create.php');
            }
            else
            {
             $upload_dir = 'user_images/';

             $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); 

          
             $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); 

       
             $userpic = rand(1000,1000000).".".$imgExt;
             
             if(in_array($imgExt, $valid_extensions)){   
              
              if($imgSize < 5000000){
               move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                if(!isset( $_SESSION['error_msg']) && empty( $_SESSION['error_msg']))
                    {
                     $stmt = $connection->prepare('INSERT INTO imageupload(userName,userProfession,userPic) VALUES(:uname, :ujob, :upic)');
                     $stmt->bindParam(':uname',$username);
                     $stmt->bindParam(':ujob',$userjob);
                     $stmt->bindParam(':upic',$userpic);

                     if($stmt->execute())
                     {
                       $_SESSION['error_msg'] = "new record succesfully inserted..";
                      header("refresh:1;create.php"); // redirects image view page after 5 seconds.
                     }
                     else
                     {
                       $_SESSION['error_msg'] = "error while inserting....";
                        header('location:create.php');
                     }
                  }
              } else{
                 $_SESSION['error_msg'] = "Sorry, your file is too large.";
                  header('location:create.php');
                }
             }
             else{
              $_SESSION['error_msg'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
               header('location:create.php');
             }
            
 }
//}else
//{
//   $_SESSION['error_msg']='404 Not found...!!!';
//    header('location:create.php');
//}