<?php
session_start();
?>
<html>
    <head>
        <title>image upload system</title>
        
        <style>
            .table1{
                width: 50%;
                margin: 0 auto;
                margin-top: 100px;
                border: 1px solid  #ff0033;
                min-height: 200px;
                padding: 30px;
            }
            .table1 tr td{
                padding: 10px;
            }
            .tdone{
                background:#cccccc;
               color: black;
            }
            .table1 input[type="text"]{
                width: 100%;
                 height: 30px;
            }
            .table1 input[type="submit"]{
                width: 100%;
                height: 35px;
                text-align: center;
                position: relative;
            }
            .table1 input[type="reset"]{
                width: 100%;
                height: 35px;
                text-align: center;
                position: relative;
            }
        </style>
    </head>
    
    <body>
        <form method="post" action="store.php" enctype="multipart/form-data" class="form-horizontal">
     
 <table class="table1">
     
     <?php if(isset(  $_SESSION['error_msg'])){ ?>
     <tr>
         <td colspan="2"><?php echo   $_SESSION['error_msg']; unset(  $_SESSION['error_msg']);?></td>
     </tr>         
    <?php } ?>
 
    <tr class="tdone">
     <td><label >Username</label></td>
        <td><input  type="text" name="user_name" placeholder="Enter Username" value="<?php echo "" ?>" /></td>
    </tr>
    
    <tr>
     <td><label class="tdtow">Profession(Job)</label></td>
        <td><input class="tdtwo" type="text" name="user_job" placeholder="Your Profession" value="<?php echo "" ?>" /></td>
    </tr>
    
    <tr class="tdone">
     <td><label>Profile Img</label></td>
        <td><input type="file" name="user_image" accept="image/*" /></td>
    </tr>
    
    <tr>
        <td colspan="2"><input type="submit" name="btnsave" value="Upload">
            <input type="reset" name="resetform" value="Reset"></td>
    </tr>
    
    </table>
    
</form>
    </body>
</html>